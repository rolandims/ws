<?php
/**
 * function a rajouter en include
 * le script appelant doit comporter le dao cassandra
 * parametres:
 * obj 
 * type = classe / member
 * mail = true / false
 * fire = true / false
 */

function alert($obj,$type = 'classe',$mail = true,$fire = false){
	global $dao;
	$result = new StdClass();
	$tokens	= [];
	require_once $_SERVER['DOCUMENT_ROOT']."/plugin/mailing/sendmail.php";
	$sendmail = new sendMail();

	$criterias = new stdClass();
	$criterias->id = $obj->classId;
	$_=$dao->find("classes","students,name",$criterias);
	foreach ($_['students'] as $k=>$v){
		$w=array($v);$v=(array($w[0]));
		foreach ($v as $a=>$b) {
			$c=(array)$b;
			if($mail){
				$criterias->id = $c['values']['id'];
				$infoStd = $dao->find("members","email,name,firstname",$criterias);
				# ENVOI PAR MAIL (OPTION PAR DEFAUT)
				$sendmail->newEvent(array(
					"email"=>$infoStd['email'],
					"prenom"=>$infoStd['firstname'].' '.$infoStd['name'],
					"classe"=>$_['name'],
					"date"=>$obj->date,
					"event"=>$obj->description,
					"title"=>$obj->title
					));
			}
			if($fire){
				unset($criterias->id);
				$criterias->ident = $c['values']['id'];
				$infoStd = $dao->findAll("tokendevice","tokenid",$criterias);
				foreach($infoStd as $item){
					$tokens[] = $item['tokenid'];
				}
			}

		}
	}
	# FIREBASE NOTIFICATION
	if($fire){
		$datas 											= new StdClass();
		$datas->tokens							= $tokens;
		$datas->notification 			= new stdClass();
		$datas->notification->title	= $obj->title;
		$datas->notification->body	=	$obj->description;

		$curl = curl_init("https://dev.imusic-school.info/firebase/sendMsg.js");
		curl_setopt($curl, CURLOPT_PORT,443);
		curl_setopt($curl, CURLOPT_POST, 1); 
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($datas));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Connection: Close")); 

		$response = curl_exec($curl);
		curl_close($curl);
		$result->fire = $datas;	
		$result->fireResp = $response;
	}

	return $result;
}
?>