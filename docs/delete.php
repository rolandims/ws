<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

$result = new stdClass();

# recup infos 

if(array_key_exists("id",$_GET)){
	$obj 					= new stdClass();
	$obj->id 			= $_GET['id'];
	$obj->folder 	= $_GET['folder'];
	$obj->name 		= $_GET['name'];
}else{
	$obj 					= json_decode(file_get_contents("php://input"));
}
if($obj==null ){die(200);}

if(($obj->id =='')||($obj->id == null)){
	$result->success = false;
	$result->message = "Missing Teacher ID";
	die(json_encode($result));
}
$file = "/home/www/vhosts/{$env->school}.imusic-school.info/app/teacher/{$obj->id}/{$obj->folder}/{$obj->name}";
if(file_exists ($file)){
	unlink($file);
	$result->success  = true;
}else{
	$result->success  = false;
	$result->message		= "file not found";
}
echo json_encode($result);


?>