<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");
$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

$activeLog = false;

# recup des données
$profId	=	$_REQUEST['profId'];
$dt_up	=	date('Y-m-d H:i:s');

# controle du fichier
if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){
	$extension	=	pathinfo($_FILES['upl']['name'],PATHINFO_EXTENSION);
	# controle du nom de fichier
	$tmpName		=	$_FILES['upl']['name'];
	$finalName		=	strtolower(preg_replace("/[^a-zA-Z0-9_\-\.]/","",$tmpName));
	
	// Log
	if ($activeLog) file_put_contents( "./upload.log", "---------------------\n$lesson $finalName $tmpName\n",FILE_APPEND );
		
		# calcul de la destination
		switch ($extension){
			#audio
			case 'mp3':
			case 'wav':
			case 'ogg':
			case 'ogv':
			case 'aif':
				$sd = 'audio';
				break;
				
			#video
			case 'mp4':
			case 'mov':
			case 'avi':
			case 'mkv':
				$sd = 'video';
				break;
				
			#score
			case 'xml':
			case 'mxl':
			case 'musicxml':
				$sd	= 'score';  
				break;
				
			#docs
			case 'doc':
			case 'rtf':
			case 'doc':
			case 'ods':
			case 'odt':
			case 'csv':
			case 'docx':
			case 'xls':
			case 'xlsx':
			case 'pdf':
			case 'eps':
			case 'jpg':
			case 'jpeg':
			case 'gif':
			case 'png':
			case 'zip':
			case 'rar':
				$sd	= 'doc';  
				break;
				
			default:       
				$sd	= 'no';  
				break;
		}
		if($sd=='no') {
			header($_SERVER["SERVER_PROTOCOL"]." 404 Not Allowed"); 
			#die('{"status":"error2"}');
		}else{
		
		$root = "/home/www/vhosts/{$env->school}.imusic-school.info/app/teacher/$profId/$sd/";

		system("mkdir -p $root;");
		
		# transfert vers le dossier cible
		if(move_uploaded_file($_FILES['upl']['tmp_name'], $root.$finalName)){
			if (file_exists($root.$finalName)) echo '{"status":"success"}';
			else 			header($_SERVER["SERVER_PROTOCOL"]." 404 Transfert Error");
			#echo '{"status":"error2"}';
		}
		else 			header($_SERVER["SERVER_PROTOCOL"]." 404 Upload Error");
		#die('{"status":"error2"}');
		}
}else{
	header($_SERVER["SERVER_PROTOCOL"]." 404 Upload Error");
	#die('{"status":"error"}');
}
?>