<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$root = "/home/www/vhosts/dev.imusic-school.info/assets/scoreTemplate/";
$fileContent = file_get_contents($root."BrassTrio.musicxml");
$debut = strpos($fileContent,'<work-title>') + strlen("<work-title>");
$fin = strpos($fileContent,'</work-title');

$findReplace = "<work-title>".substr($fileContent,$debut,$fin-$debut)."</work-title>";
$toReplace = "<work-title>SCORENAME</work-title>";
echo str_replace($findReplace,$toReplace,$fileContent);

?>