<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));


require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra("SRV_CASSANDRA_IMS",$env->keyspace);

$result = new stdClass();

# recup infos 

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if(($obj->id =='')||($obj->id == null)){
	$result->success = false;
	$result->message = "Missing Teacher ID";
	die(json_encode($result));
}
$root = "/home/www/vhosts/{$env->school}.imusic-school.info/app/teacher/".$obj->id."/score/";
system("mkdir -p $root");
file_put_contents($root.$obj->scoreName,$obj->datas);
$result->success = true;
$result->path = "https://{$env->school}.imusic-school.info/app/teacher/".$obj->id."/score/".$obj->scoreName;
echo json_encode($result);
?>