<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");
$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

$retour = new stdClass();

if(array_key_exists("id",$_GET)) {$obj =new stdClass();$obj->id= $_GET['id'];}
else 	$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if(($obj->id ==null) || ($obj->id=="")){
	$retour->success = false;
	$retour->message = "Teacher ID missing";
	header('Content-Type: application/json');
	die(json_encode($retour));
}
require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra("SRV_CASSANDRA_IMS",$env->keyspace);
$_ = $dao->find("members","*",$obj);
if($_['profile']=='Student'){
	$retour->success	= false;
	$retour->message	= "not a teacher";
	header('Content-Type: application/json');
	die(json_encode($retour));
}

?>
<html>
<body>
<head>
<meta charset="utf-8"/>
<!-- Google web fonts -->
<link href="/assets/fonts/PT_Sans_Narrow/imports.css" rel='stylesheet' />
<!-- The main CSS file -->
<link href="/assets/css/uploadcss/css/style.css" rel="stylesheet" />
<link href="upload.css" rel="stylesheet" />
<!-- JavaScript Includes -->
<script src="/assets/plugins-js/jquery/jquery-1.9.1.min.js"></script>
<script src="/assets/plugins-js/upload/jquery.knob.js"></script>
<!-- jQuery File Upload Dependencies -->
<script src="/assets/plugins-js/upload/jquery.ui.widget.js"></script>
<script src="/assets/plugins-js/upload/jquery.iframe-transport.js"></script>
<script src="/assets/plugins-js/upload/jquery.fileupload.js"></script>
<!-- Our main JS file -->
<script src="/assets/js/script.js"></script>
</head>	<div> 
	<form id="upload" method="post"
		action="upload.php?<? echo "profId={$obj->id}&t=" . date ( 'Y-m-d-h-i-s' );?>
		enctype="multipart/form-data">
		<div id="drop">
			<?php echo $_['id'];?> 
			Déposez ici vos fichiers<br>
			<span style="color:white">Formats audio:</span> mp3, wav, ogg, ogv, aif<br>
			<span style="color:white">Formats video:</span> mp4, mov, avi, mkv<br>
			<span style="color:white">Partitions:</span> xml, mxl, musicxml<br>
			<span style="color:white">Documents:</span> txt, doc, docx, xls, xlsx, pdf, eps, jpg, jpeg, gif, png, zip, rar<br>
			<a>Parcourir...</a> <input type="file" name="upl"
				multiple />
		</div>
		<ul>
			<!-- The file uploads will be shown here -->
		</ul>
	</form>
	</div>
</body>
</html>
