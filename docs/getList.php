<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));


require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra("SRV_CASSANDRA_IMS",$env->keyspace);

$result = new stdClass();

# recup infos professeur

if(array_key_exists("id",$_GET)){
	$obj = new stdClass();
	$obj->id = $_GET['id'];
}else{
	$obj = json_decode(file_get_contents("php://input"));
}
if($obj==null ){die(200);}

if(($obj->id =='')||($obj->id == null)){
	$result->success = false;
	$result->message = "Missing Teacher ID";
	die(json_encode($result));
}
$_ = $dao->find("members","*",$obj);
/* TODO A DECOMMENTER QD TOUT EST OK */
if($_['profile']!='Teacher' && $_['profile']!='Admin'){
	$result->success	= false;
	$result->message	= "not a teacher";
	die(json_encode($result));
}	

$root = "/home/www/vhosts/{$env->school}.imusic-school.info/app/teacher/".$obj->id;

echo json_encode(folderList($root,''));

function folderList($pFolder,$pSub) {
	$return = [];
	if(!is_dir($pFolder)) return $return;
	foreach (array_diff(scandir($pFolder),array(".","..")) as $k=>$v){
		if(is_dir("$pFolder/$v")){
			$return[$v]=folderList("$pFolder/$v",$v);
		}else{
			
			if($v!='avatar.png' && $v!='avatar.jpg'){
				$metas = [];
				$info = pathinfo("$pFolder/$v");
				switch ($pSub){
					case 'audio':
					case 'video':
						exec("exiftool -TAG -All $pFolder/$v",$metas,$errorcode);
						break;
					case 'score':
						exec("sed -n '/work-title/{s/.*<work\-title>//;s/<\/work\-title.*//;p;}' $pFolder/$v",$title,$errorcode);
						//exec("sed -n '/movement-title/{s/.*<movement\-title>//;s/<\/movement\-title.*//;p;}' $pFolder/$v",$title,$errorcode);
						$metas[]="Title:'". $title[0]."'";
						break;
					case 'doc':
						switch(strtolower($info['extension'])){
							case 'pdf':
								exec("pdfinfo $pFolder/$v",$metas,$errorcode);
								break;
							case 'eps':
							case 'jpg':
							case 'jpeg':
							case 'gif':
							case 'png':
								exec("identify -verbose $pFolder/$v",$metas,$errorcode);
								break;
							case 'doc':
							default:
								exec("exiftool -TAG -All $pFolder/$v",$metas,$errorcode);
						}
						break;
					default:
						exec("exiftool -TAG -All $pFolder/$v",$metas,$errorcode);
				}
				
				$return[]=array(
						"name"=>$v,
						"url"=>str_replace('/home/www/vhosts/','https://',"$pFolder/$v"),
						"createTime"=>date("Y-m-d H:i:s", filemtime("$pFolder/$v")),
						"folder"=>$pSub,
						"metas"=>$metas
				);
			}
		}
	}
	return $return;
}

?>