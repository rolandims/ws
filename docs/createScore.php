<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));


require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra("SRV_CASSANDRA_IMS",$env->keyspace);

$result = new stdClass();

# recup infos 

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if(($obj->teacher =='')||($obj->teacher == null)){
	$result->success = false;
	$result->message = "Missing Teacher ID";
	die(json_encode($result));
}
$obj->targetFileName = str_replace(' ','_',$obj->targetFileName);
$root = "/home/www/vhosts/{$env->school}.imusic-school.info/app/teacher/".$obj->teacher."/score/";
$src  = "/home/www/vhosts/{$env->school}.imusic-school.info/assets/scoreTemplate/{$obj->fileName}.musicxml";
$target  = "$root{$obj->targetFileName}.musicxml";
if(!file_exists($target)){
	system("mkdir -p $root; cp $src $target;");
	$result->success = true;
	$result->path = "https://{$env->school}.imusic-school.info/app/teacher/".$obj->teacher."/score/{$obj->targetFileName}.musicxml";
}else{
	$result->success = false;
	$result->message = "File already exists";
}

echo json_encode($result);
?>