<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

# recup infos 

# <work-title>Drums</work-title>
$result = new stdClass();
$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if(($obj->id =='')||($obj->id == null)){
	$result->success = false;
	$result->message = "Missing Teacher ID";
	die(json_encode($result));
}
$fileName = "/home/www/vhosts/{$env->school}.imusic-school.info/app/teacher/".$obj->id."/score/".$obj->scoreName.".musicxml";
$fileContent = file_get_contents($fileName);
$debut = strpos($fileContent,'<work-title>') + strlen("<work-title>");
$fin = strpos($fileContent,'</work-title');

$findReplace = "<work-title>".substr($fileContent,$debut,$fin-$debut)."</work-title>";
$toReplace = "<work-title>".$obj->title."</work-title>";
$newFile = str_replace($findReplace,$toReplace,$fileContent);

# <credit-words>Brass Quartet</credit-words>
$debut = strpos($newFile,'<credit-words>') + strlen("<credit-words>");
$fin = strpos($newFile,'</credit-words>');
$findReplace = "<credit-words>".substr($newFile,$debut,$fin-$debut)."</credit-words>";
$toReplace = "<credit-words>".$obj->title."</credit-words>";
$newFile = str_replace($findReplace,$toReplace,$newFile);

file_put_contents($fileName,$newFile);
$result->message = $fileName;
$result->success = true;
echo json_encode($result);
?>