<?php 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$result = new stdClass();

# recup liste classes, profs et eleves

$classes = $dao->executeTxt("SELECT id,instrument, teacher,name,students FROM classes;");
foreach ($classes as $class){
	$_							=	new stdClass();
	$_->roomId 			= $class['id'];
	$_->adminId 		= $class['teacher'];
	$_->instrument 	= $class['instrument'];
	$_->isactive 		= true;
	$_->name 				= $class["name"];
	$_->usersList		=	'["'.$class['teacher'].'","';
	foreach ($class['students'] as $user) {
		foreach($user as $k=>$v){
			if($k=='id'){
				$_->usersList		.=	"$v\",\"";
			}
		}
	}
	$_->usersList		=	substr($_->usersList,0,-2).']';
	if($_->usersList==']')$_->usersList='[]';
	
	$dao->insertJSON("rooms",$_);
	echo "<pre>";print_r($_);echo "</pre>",$dao->message,"\n";
}

?>