<?php 

require "vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/plugin/connect/Dao.php";

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

class vonage {
	public $error;
	public $message;
	public $session;
	public $token;
	public $openTok;
	private $dao;
  	public $apiKey;
	private $apiSecret;
	
	public function __construct() {
		
		$this->apiKey = "46859144";
		$this->apiSecret = "eab510f67d9093e29d5a753aa57d0dc7a4fe1c6d";
		
		$this->opentok 	= new OpenTok($this->apiKey, $this->apiSecret);
		$this->dao		= new Dao("PROD","vonage"); 
	}
	
	
	/* SESSION MANAGER */
	public function newSession($pType) {
		switch ($pType) {
			case 0:
				// A session that uses the OpenTok Media Router, which is required for archiving:
				$this->session = $this->opentok->createSession(array( 'mediaMode' => MediaMode::ROUTED ));
			break;
			case 1:
				// A session with a location hint:
				$this->session = $this->opentok->createSession(array( 'location' => '62.210.93.238' ));
			break;
			case 2:
				// An automatically archived session:
				$sessionOptions = array( 'archiveMode' => ArchiveMode::ALWAYS, 'mediaMode' => MediaMode::ROUTED);
				$this->session = $this->opentok->createSession($sessionOptions);
			break;
			
			default:
				// Create a session that attempts to use peer-to-peer streaming:
				$this->session = $this->opentok->createSession();	
			break;
		}

		// Store this sessionId in the database for later use
		return $this->session->getSessionId();	
	}
	public function getCurrentSessionId(){ return $this->session->getSessionId(); }
	
	public function setCurrentSessionId($pId){
		 $this->session->getSessionId();
	}
	
	public function newToken ($pStudent,$pSession=null){
		$session = ($pSession==null) ?$this->session:$pSession;
		
		$role = array_search($pStudent->type, array(Role::MODERATOR=>'MODERATOR', Role::SUBSCRIBER=>'SUBSCRIBER', Role::PUBLISHER=>'PUBLISHER'));
		
		// Generate a Token from just a sessionId (fetched from a database)
//		$this->token = $this->opentok->generateToken($this->session->getSessionId());
//		$this->token = $this->opentok->generateToken($session);
		
		// Generate a Token by calling the method on the Session (returned from createSession)
//		$this->token = $this->session->generateToken();
	
		
		// Set some options in a token
		$this->token = $this->session->generateToken(array(
				'role'       => $role, // PUBLISHER/SUBSCRIBER/MODERATOR
				'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
				'data'       => 'name='.$pStudent->ident,
				'initialLayoutClassList' => array('focus')
		));
	}
	
	public function createToken ($pStudent,$pSession){
		
		$opentok = new OpenTok("46859144","eab510f67d9093e29d5a753aa57d0dc7a4fe1c6d");
		// Replace with meaningful metadata for the connection:
		$connectionMetaData = "username=$pStudent,userLevel=4";
		// Replace with the correct session ID:
		return $opentok->generateToken($pSession, 
				array('role' => Role::PUBLISHER, 
						'expireTime' => time()+(7 * 24 * 60 * 60), 
						'data' =>  $connectionMetaData));
		
	}


}

?>