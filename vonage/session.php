<?php 
require "vendor/autoload.php";

$apiKey = "46859144";
$apiSecret = "eab510f67d9093e29d5a753aa57d0dc7a4fe1c6d";

use OpenTok\OpenTok;

$opentok = new OpenTok($apiKey, $apiSecret);

use OpenTok\MediaMode;
use OpenTok\ArchiveMode;

// Create a session that attempts to use peer-to-peer streaming:
$session = $opentok->createSession();

// A session that uses the OpenTok Media Router, which is required for archiving:
$session = $opentok->createSession(array( 'mediaMode' => MediaMode::ROUTED ));

// A session with a location hint:
$session = $opentok->createSession(array( 'location' => '62.210.93.238' ));

// An automatically archived session:
$sessionOptions = array(
		'archiveMode' => ArchiveMode::ALWAYS,
		'mediaMode' => MediaMode::ROUTED
);
$session = $opentok->createSession($sessionOptions);


// Store this sessionId in the database for later use
$sessionId = $session->getSessionId();

?>