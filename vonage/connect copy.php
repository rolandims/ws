<?php 

require_once 'class.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/plugin/connect/Dao.php';
$dao = new Dao("PROD","vonage");
$vonage 		= new vonage();
$ret 			= new stdClass();
if(!array_key_exists('room',$_GET) OR !array_key_exists('ident',$_GET) ){
	$ret->success = false;
	$ret->message = "Missing Parameters !";
	$ret->p		  = $_GET;
	die(json_encode($ret));
}
$ident		 	= $_GET['ident'];
$room			= $_GET['room'];

$confUser	= $dao->find("SELECT u.token,r.idSession,connected FROM vonage.confUser u, vonage.room r WHERE r.roomId='$room' AND r.roomId=u.roomId AND ident='$ident'");
if($confUser['token']==null){
	$search = $dao->find("SELECT u.token,r.idSession FROM vonage.confUser u, vonage.room r WHERE r.roomId='$room' AND r.roomId=u.roomId AND ident LIKE 'PUBLISHER+%' limit 1;");
	if($search['token']==null){
		$ret->success = false;
		$ret->message = "No token found";
		$search = $dao->find ("SELECT * FROM room WHERE roomId='$room';");
		$token = $vonage->createToken($ident,$search['idSession']);
		
		$dao->insertData("INSERT INTO confUser SET ident='$ident',roomId='$room',role=1,token='$token', connected=1;");

		$ret->token 			= $token;
		$ret->sessionId 	= $search['idSession'];
	
	}else{
		$ret->token 			= $search['token'];
		$ret->sessionId 	= $search['idSession'];
		$dao->updateData("UPDATE vonage.confUser SET ident='$ident', connected=1 WHERE token='{$search['token']}' AND roomId='$room';");
	}
}else{
	/*if($confUser['connected']==1){
		$ret->success = false;
		$ret->message = "User connected";
		die(json_encode($ret));
	}*/
	$dao->updateData("UPDATE vonage.confUser SET connected=1 WHERE token='{$confUser['token']}';");	
	$ret->token 			= $confUser['token'];
	$ret->sessionId 	= $confUser['idSession'];
}

$ret->success 		= true;
$ret->apiKey			=	$vonage->apiKey;

die(json_encode($ret));
?>