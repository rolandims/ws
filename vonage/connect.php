<?php 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if($_SERVER['QUERY_STRING']!='') {
	$obj = json_decode($_SERVER['QUERY_STRING']);
}
else 	{
	$obj = json_decode(file_get_contents("php://input"));
}

$ret = new stdClass();

if(!isset($obj->ident)||!isset($obj->eventId)){
	$ret->success = false;
	$ret->message = "Error send datas !";
	echo json_encode($ret);
	die();
}
$ident = $obj->ident;
$eventId = $obj->eventId;
//$ident = 'rolandimusic';
//$eventId = '4dad1838-8dc6-4b46-85b5-8bf301345edf';

require_once 'class.php';
$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));
require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);
$vonage 		= new vonage();


$event = $dao->executeTxt("SELECT * FROM workevents WHERE id=$eventId;");
$event = $event[0];
if(!isset($event['id'])){
	$ret->success = false;
	$ret->message = "Event Not Found !";
	//echo '<pre>';print_r($event);
	echo json_encode($ret);die();
}

//echo "FOUND EVENT"."<br>";
//echo "room id : ".$event['roomid'];

if($event['roomid']==NULL || strlen($event['roomid'])<5){
	//echo " - NOT SET - "."<br>";
	// CREATE ROOM
    $sessionId = $vonage->newSession(0);
	
	$room = uniqid (rand (), true);
	$query = "INSERT INTO vonageroom (roomid, admin, flux, idsession, maxuser) VALUES ('$room', '$ident', 0, '$sessionId', 4);";

	//echo $query;
	$dao->executeTxt($query);
	//echo $dao->message;
	// UPDATE WORKEVENT
	$query = "UPDATE workevents SET roomid='$room' WHERE id=$eventId;";
	$dao->executeTxt($query);
	// CREATE TOKEN
	$userToken = $vonage->createToken($ident,$sessionId);
	$uuid = new Cassandra\UUID();
	$query = "INSERT INTO vonageuser (id, connected, ident, roomid, usertoken) VALUES ($uuid, 1, '$ident', '$room', '$userToken');";
	$dao->executeTxt($query);
}else{
	$room = $event['roomid'];
	//echo " - SET - $room"."<br>";
	$vonageRoom = $dao->executeTxt("SELECT * FROM vonageroom WHERE roomid='$room';");
	$vonageRoom = $vonageRoom[0];
	$sessionId= $vonageRoom['idsession'];
	//echo "idsession : ".$sessionId." - ".$vonageRoom['roomid'];
	if($sessionId==NULL || strlen($sessionId)<5){
		$sessionId = $vonage->newSession(0);
		$idForUpdateRoom = $vonageRoom['roomid'];
		$query = "UPDATE vonageroom SET idsession='$sessionId' WHERE roomid='$idForUpdateRoom';";
		$dao->executeTxt($query);
		// echo "<h3>$query</h3>";
	}
	$vonageuser = $dao->executeTxt("SELECT * FROM vonageuser WHERE ident='$ident' AND roomid='$room' ALLOW FILTERING;");
	$vonageuser = $vonageuser[0];
	if(isset($vonageuser['id'])){
		$userToken = $vonageuser['usertoken'];
		// echo "<h3> USER TOKEN  SET / $userToken</h3>";
	}else{
		$userToken = $vonage->createToken($ident,$sessionId);
		$uuid = new Cassandra\UUID();
		$query = "INSERT INTO vonageuser (id, connected, ident, roomid, usertoken) VALUES ($uuid, true, '$ident', '$room', '$userToken');";
		//echo "<h3>$query</h3>";
		$dao->executeTxt($query);
	}
}

$ret->success 		= true;
$ret->apiKey		= $vonage->apiKey;
$ret->sessionId 	= $sessionId;
$ret->roomId 		= $room;
$ret->token 		= $userToken;
$ret->admin 		= $event['admin'];

die(json_encode($ret));
?>