<?php 
header("Content-Type: application/json; charset=UTF-8");
$postObj 				= json_decode(file_get_contents('php://input'));

$ident		 	= $postObj->ident;
$room			= $postObj->seq;

require_once 'class.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/plugin/connect/Dao.php';
$dao = new Dao("PROD","vonage");
$vonage 		= new vonage();
$ret 			= new stdClass();
if($ident=NULL OR $room==NULL ){
	$ret->success = false;
	$ret->message = "Missing Parameters !";
	$ret->p		  = $_GET;
	die(json_encode($ret));
}


$confUser	= $dao->find("SELECT u.token,r.idSession,connected FROM vonage.confUser u, vonage.room r WHERE r.seq='$room' AND r.roomId=u.roomId AND ident='$ident'");
if($confUser['token']==null){
	$ret->m1 = "SELECT u.token,r.idSession,connected FROM vonage.confUser u, vonage.room r WHERE r.seq='$room' AND r.roomId=u.roomId AND ident='$ident'";
	$search = $dao->find("SELECT u.token,r.idSession FROM vonage.confUser u, vonage.room r WHERE r.seq='$room' AND r.roomId=u.roomId AND ident LIKE 'PUBLISHER+%' limit 1;");
	if($search['token']==null){
		$ret->success = false;
		$ret->message = "No token found";
		$search = $dao->find ("SELECT * FROM room WHERE seq='$room';");
		$token = $vonage->createToken($ident,$search['idSession']);
		$roomId = $search['roomId'];
		$dao->insertData("INSERT INTO confUser SET ident='$ident',roomId='$roomId',role=1,token='$token', connected=1;");

		$ret->token 		= $token;
		$ret->sessionId 	= $search['idSession'];
	
	}else{
		$ret->token 		= $search['token'];
		$ret->sessionId 	= $search['idSession'];
		$dao->updateData("UPDATE vonage.confUser SET ident='$ident', connected=1 WHERE token='{$search['token']}';");
	}
}else{
	$dao->updateData("UPDATE vonage.confUser SET connected=1 WHERE token='{$confUser['token']}';");	
	$ret->token 			= $confUser['token'];
	$ret->sessionId 	= $confUser['idSession'];
}
$ret->currentRoom	= $dao->find("SELECT * FROM vonage.room r WHERE r.seq='$room';");
$ret->success 		= true;
$ret->apiKey		= $vonage->apiKey;

die(json_encode($ret));
?>