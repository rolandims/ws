<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

require_once 'class.php';
$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));
require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";

$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$vonage 		= new vonage();
$user 			= new stdClass();
$ret 			= new stdClass();

$ident		 	= $_GET['ident'];

$room			= $_GET['room']."-".date('YmdHis');
if(!array_key_exists('maxuser',$_GET)){
	$maxUser = 4;
}else{
	$maxUser  = $_GET['maxuser'];
}



$sessionId = $vonage->newSession(0);
$query = "INSERT INTO vonageroom SET roomId='$room', admin='$ident',flux=0,idSession='$sessionId',`type`=2, url='', maxUser=$maxUser, start=now(), end=adddate(now(), interval 1 day);";
$dao->executeTxt($query);


# MODERATOR
//$user->ident		=	$ident;
//$user->type		= 'MODERATOR';
//$vonage->newToken($user,$sessionId);
//$token					= $vonage->token;
//$dao->insertData("INSERT INTO vonage.confUser SET roomId='$room', ident='$ident', role='0', token='$token', connected=0 " )	;
/*
for($i =1; $i<$maxUser;$i++){
	$user->ident		=	'user_'.$i;
	$user->type		= 'SUBSCRIBER';
	$vonage->newToken($user,$sessionId);
	$token					= $vonage->token;
	$dao->insertData("INSERT INTO vonage.confUser SET roomId='$room', ident='PUBLISHER+$i', role='1',token='$token',connected=0 ")	;
}*/

$ret->sessionId = $sessionId;
$ret->success = true;
$ret->message = "Session $room created";
die (json_encode($ret));

?>