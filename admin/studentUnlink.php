<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra("SRV_CASSANDRA_IMS",$env->keyspace);

$result = new stdClass();
$result->exec='';
$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if(property_exists($obj,"teacherId")){
	$result->exec = $obj->teacherId."::";
	foreach($obj->studentList as $studentId){
		$dao->executeTxt("DELETE FROM studentteacher WHERE studentId='$studentId' AND teacherId='{$obj->teacherId}';");
		$result->exec.=$studentId.":";
	}
}elseif (property_exists($obj,"studentId")){
	$result->exec = $obj->studentId."::";
	foreach($obj->teacherList as $teacherId){
		$dao->executeTxt("DELETE FROM studentteacher WHERE studentId='{$obj->studentId}' AND teacherId='$teacherId';");
		$result->exec.=$teacherId.":";
	}
}

if($dao->error){
	$result->success = false;
	$result->message = "Error unlinking";
}else{
	$result->success = true;
	$result->message = "Unlink done";
}
echo json_encode($result);
?>