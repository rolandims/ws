<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra("SRV_CASSANDRA_IMS",$env->keyspace);

$result = new stdClass();

# recup infos professeur

if(array_key_exists("id",$_GET)){
	$obj = new stdClass();
	$obj->id = $_GET['id'];
}else{
	$obj = json_decode(file_get_contents("php://input"));
}
if($obj==null ){die(200);}

if(($obj->id =='')||($obj->id == null)){
	$result->success = false;
	$result->message = "Missing Admin ID";
}
$_ = $dao->find("members","*",$obj);
if($_['profile']!='Admin'){
	$result->success	= false;
	$result->message	= "not a Admin";
}else{
	$contact = (array)$_['contact'];
	$result->teacher = new stdClass();
	$result->teacher->name 				= $contact['values']['name'];	
	$result->teacher->firstname 	= $contact['values']['firstname'];
	$result->teacher->email 			= $contact['values']["email"];
	$b = (array)$contact['values']['birthday'];
	$result->teacher->birthday 		= date('Y-m-d',$b['seconds']);
	$result->teacher->instrument	= [];
	$result->teacher->profileType	= $_['profile'];
	$result->teacher->roles	= $_['roles'];
	$result->teacher->valid	= $_['valid'];
}
echo json_encode($result);
?>