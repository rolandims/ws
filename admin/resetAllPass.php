<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/mailing/sendmail.php";
$sendmail = new sendMail();


$result = new stdClass();

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if(array_key_exists("email",$_GET)){$obj = new stdClass();$obj->email=$_GET['email'];}

$criteria = new stdClass();
$criteria = $obj->email;
$search = $dao->executeTxt("SELECT * FROM members WHERE profile='Student' AND id='JCVD' ALLOW FILTERING;");
foreach ($search as $_) {

# mot de passe aléatoire
	$p1 = array ('do','re','mi','sol','fa','la','si');
	$p2 = array ('ronde','croche','noire','blanche');
	$v3 = round ( rand ( 0, 99 ), 0 );
	$v1 = array_rand ( $p1 );
	$v2 = array_rand ( $p2 );
	$password = $p1 [$v1] .$v3. $p2 [$v2] ;
	$crypted = password_hash($password,PASSWORD_BCRYPT,['cost'=>10]);
	
	$m = $dao->executeTxt("UPDATE members SET pass ='$crypted' WHERE id='{$_['id']}'; ");
	if($dao->error){
		$result->success = false;
		$result->message = "erreur de modification de mot de passe";
		$result->detail = "UPDATE members SET pass ='$crypted' WHERE id='{$_['id']}'; ";
	}else{
		$result->success = true;
		$result->message = "mot de passe modifié et envoyé";
		$sendmail->resetPassword(array("ident"=>$_['id'],"prenom"=>$_['firstname'],"pass"=>$password,"email"=>$_['email']));
	}
}
echo json_encode($result);