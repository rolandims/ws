<?php 
$sentDatas = json_decode(file_get_contents("php://input"));
if($sentDatas==null ){die(200);}

$result 			= new stdClass();
$result->success 	= true;
$filename           = $_SERVER['DOCUMENT_ROOT']."/env.json";
$env        		= json_decode(file_get_contents($filename));

foreach ($env as $key => $value) {
    if($key!="school" && $key!="keyspace" &&  $key!="sender" && $key!="version" ){
        if(property_exists($sentDatas,$key)){
            $env->$key = $sentDatas->$key;
           }
    }
}
$handle 			= fopen($filename, "w+");
if(!$handle) {
	// ERROR DOSSIER OU DROITS INEXISTANTS
	$result["success"]	= false;
	$result["msgId"]	= "MESSAGE.SERVER.ERR14";
}else{
    fwrite( $handle,json_encode($env) );
    fclose( $handle );  
}

echo json_encode($result);
?>