<?php 

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));
unset($env->keyspace);
unset($env->school);
unset($env->sender);
unset($env->version);
echo json_encode($env);
?>