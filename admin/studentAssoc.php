<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra("SRV_CASSANDRA_IMS",$env->keyspace);

$result = new stdClass();

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if(property_exists($obj,"teacherId")){
	foreach($obj->studentList as $studentId){
		$dao->executeTxt("INSERT INTO studentteacher (studentId,teacherId) VALUES('$studentId','{$obj->teacherId}');");
	}
}elseif (property_exists($obj,"studentId")){
	foreach($obj->teacherList as $teacherId){
		$dao->executeTxt("INSERT INTO studentteacher (studentId,teacherId) VALUES('{$obj->studentId}','$teacherId');");
	}
}

if($dao->error){
	$result->success = false;
	$result->message = "Error associating";
}else{
	$result->success = true;
	$result->message = "";
}
echo json_encode($result);
?>