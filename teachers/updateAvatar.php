<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$result = array ();

$sourceDir = $_SERVER ['DOCUMENT_ROOT'] . "/app/teacher/";

//file_put_contents ( "log.log", var_export ( $_POST, true ), FILE_APPEND );

	$id = $_POST['id'];
	$image = explode ( ",", $_POST['file'] );

	$real_data = $image [1]; // substr($image, strpos($image, ",") + 1); // We remove the header (file type and encoding type)
	$real_data = str_replace ( ' ', '+', $real_data );
	$decoded = base64_decode ( $real_data.'' ); // We decode the data because is base64 encoded

	$path = $sourceDir . $id . "/avatar.png";

	$result ["url"] = $path;

	if (! is_dir ( $sourceDir . $id )) {
		system ( "cp -r $sourceDir/template/ $sourceDir/$id/" );
	}

	$file = fopen ( $path, "wb" );
	fwrite ( $file, $decoded );
	fclose ( $file );
	system("cp $sourceDir/$id/avatar.png $sourceDir/../avatar/$id.png");
	

echo json_encode ( $result );

?>