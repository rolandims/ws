<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$result = new stdClass();
$obj = new stdClass();

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

//$obj->id=$_GET['id'];

# verif droit suppression: type admin uniquement
$criterias = new stdClass();
$criterias->id = $obj->user;
$_ = $dao->find("members","profile,roles",$criterias);

if((($_['profile']=='Teacher')&& ($_['roles']==3))||($_['profile']=='Admin')) {
	$result->success = true;
	$result->message = "You are authorized to delete a teacher";
}else{
	$result->success = false;
	$result->message = "You are not authorized to delete a teacher";
	die(json_encode($result));
}

# suppression du prof dans les liaisons avec eleves
$_ = $dao->executeTxt("DELETE FROM studentteacher WHERE teacherid ='{$obj->id}';");

# suppression du prof dans les classes
$_ = $dao->executeTxt("UPDATE classes SET teacher ='' WHERE teacher='{$obj->id}';");

# suppression fiche prof
$_ = $dao->executeTxt("DELETE from members WHERE id='{$obj->id}';");


if($dao->error){
	$result->success = false;
	$result->message = "Error deleting member";
	$result->error = $dao->message;
}else{
	$result->success = true;
	$result->message = $obj->id." deleted from members and classes ".implode(',',$result->classes);
}
echo json_encode($result);
?>

