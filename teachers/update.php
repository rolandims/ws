<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$result = new stdClass();

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

$search = $obj->search;
$datas = $obj->datas;
if(!property_exists($search,"id")){
	$result->success = false;
	$result->message = "missing member id";
}else{
	$criterias = new stdClass();
	$criterias->id = "{$search->id}";
}

# cryptage pass si transmis
/**
 * ATTENTION:
 * NE JAMAIS TRANSMETTRE UN MOT DE PASSE S'IL EST INCHANGE
 * TRANSMETTRE LE PASS EN CLAIR POUR CRYPTAGE INTERNE
 */

if(property_exists($datas,"pass")){
	$pass= $datas->pass;
	$datas->pass = password_hash($datas->pass,PASSWORD_BCRYPT,['cost'=>10]);
}

if(property_exists($datas,"contact")){
	$contact=[];
	foreach ((array)$datas->contact as $k=>$v){
		$contact[]="$k:'$v'";
	}
	$datas->contact = '{'.implode(',',$contact).'}';
}
if(property_exists($datas,"contact2")){
	$contact=[];
	foreach ((array)$datas->contact2 as $k=>$v){
		$contact[]="$k:'$v'";
	}
	$datas->contact2 = '{'.implode(',',$contact).'}';
}

if(property_exists($datas,"address")){
	if(count($datas->address)==0)$datas->address="{name:'',street:'',state:'',zipcode:'',country:'',town:'',id:''}";
}


//$dao->updateJSON("members",$datas,$criterias);


$req = $dao->getUpdateRequest("members",$datas);
$reqTxt = "UPDATE members SET $req WHERE id='".$search->id."';";
$result->query = $reqTxt;
$dao->executeTxt($reqTxt);

if($dao->error){
	$result->success = false;
	$result->message = "Error updating member";
	$result->detail = $dao->message;
	$result->datas = $datas;
	$result->req = $req;
}else{
	$result->success = true;
	$result->message = $search->id." updated";
}
echo json_encode($result);
?>
