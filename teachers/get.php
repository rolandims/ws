<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra("SRV_CASSANDRA_IMS",$env->keyspace);

$result = new stdClass();

# recup infos professeur

if(array_key_exists("id",$_GET)){
	$obj = new stdClass();
$obj->id = $_GET['id'];
}else{
	$obj = json_decode(file_get_contents("php://input"));
}

if($obj==null ){die(200);}

//file_put_contents("./log.log",var_export($obj,true),FILE_APPEND);
if(($obj->id =='')||($obj->id == null)){
	$result->success = false;
	$result->message = "Missing Teacher ID";
}
$_ = $dao->find("members","*",$obj);
if($_['profile']=='Student'){
	$result->success	= false;
	$result->message	= "not a teacher nor an admin";
}else{
	$result->teacher = $_;
	$contact = (array)$_['contact'];
	$result->teacher = new stdClass();
	$result->teacher->name 				= $_['name'];	
	$result->teacher->firstname 	= $_['firstname'];
	$result->teacher->email 			= $_["email"];
	$result->teacher->birthdate		= $_['birthdate'];
	$result->teacher->phone 			= $_['phone'];
	$result->teacher->profileType	= $_['profile'];
	$address = (array)$_['address'];
	$result->teacher->address 		= $address;
	if(array_key_exists("values",$address)){
		$result->teacher->zipcode = $address['values']['zipcode'];
		$result->teacher->town 		= $address['values']['town'];
		$result->teacher->street 	= $address['values']['street'];
	}else{
		$result->teacher->zipcode = "";
		$result->teacher->town 		= "";
		$result->teacher->street 	= "";
	}
	$result->teacher->instruments	= $_['instruments'];
	$result->teacher->valid 			= $_['valid'];
	$result->teacher->roles 			= $_['roles'];
	/*$instruments = (array)$_['instrument'];
	foreach ($instruments['values'] as $k=>$v) {
		$result->teacher->instrument[$k]=$v;
	}*/
	
	# recup infos classes
	$criterias = new stdClass();
	$criterias->teacher = "$obj->id";
	$classes = $dao->findAll("classes","id,students,name,instrument",$criterias);
	$class = [];
	$student = [];
	foreach ($classes as $value) {
		$class[]=array('id'=>$value['id'],'name'=>$value['name'],'instrument'=>$value['instrument']);
	/*	if($value['students']!=null ){
			foreach ($value['students'] as $s) {
				foreach($s as $k=>$v){
					if($k=='id'){
						$search = $dao->executeTxt("SELECT id,name,firstname,instruments FROM members WHERE id='$v' ALLOW FILTERING; ");
						$instr=null; foreach ((array)$search[0]['instruments'] as $a=>$b ){ if ($a=='values') $instr=$b;}
						$student[]=array('id'=>$search[0]['id'],'name'=>$search[0]['name'],'firstname'=>$search[0]['firstname'],'instruments'=>$instr);
					}
				}
			}
		}*/
	}
	$criterias = new stdClass();
	$criterias->teacherId = "$obj->id";
	$students= $dao->findAll("studentteacher","*",$criterias);

	//$students = $dao->executeTxt("SELECT studentId FROM studentteacher WHERE teacherId='{$obj->id}' ALLOW FILTERING;");
	foreach ($students as $value){
		$search = $dao->executeTxt("SELECT id,name,firstname,instruments FROM members WHERE id='{$value['studentid']}' ALLOW FILTERING; ");
		$instr=null; foreach ((array)$search[0]['instruments'] as $a=>$b ){ if ($a=='values') $instr=$b;}
		$student[]=array('id'=>$search[0]['id'],'name'=>$search[0]['name'],'firstname'=>$search[0]['firstname'],'instruments'=>$instr);
	}
	
	$root = "/home/www/vhosts/{$env->school}.imusic-school.info/app/teacher/".$obj->id;
	
	
	$result->classes 	= $class;
	$result->students = $student;
	$result->documents = folderList($root);
	$result->cours		= "";
}

function folderList($pFolder) {
	$return = [];
	foreach (array_diff(scandir($pFolder),array(".","..")) as $k=>$v){
		if(is_dir("$pFolder/$v")){
			$return[$v]=folderList("$pFolder/$v");
		}else{
			
			$return[]=array("name"=>$v,"url"=>str_replace('/home/www/vhosts/','https://',"$pFolder/$v"));
		}
	}
	return $return;
}
echo json_encode($result);
?>