<?php 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra("SRV_CASSANDRA_IMS",$env->keyspace);

$criteria = json_decode(file_get_contents("php://input"));
$criteria = New StdClass();
$criteria->profile = "Teacher";
echo json_encode($dao->findAll("members","*",$criteria));
die();
echo $dao->message;
echo $dao->error;
?>