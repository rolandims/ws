<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$result = new stdClass();

$obj = json_decode(file_get_contents("php://input"));

if($obj==null ){die(200);}

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/mailing/sendmail.php";
$sendmail = new sendMail();

$obj->profile = 'Teacher';
if(!property_exists($obj,'valid')) $obj->valid = true;
if(!property_exists($obj,'roles')) $obj->roles = 0;
# cryptage pass
$p1					= array ('do','re','mi','sol','fa','la','si');
$p2					= array ('ronde','croche','noire','ton');
$v3 				= round ( rand ( 0, 99 ), 0 );
$v1 				= array_rand ( $p1 );
$v2 				= array_rand ( $p2 );
$pwd 				= $p1 [$v1] . $v3 . $p2 [$v2];

if(!property_exists($obj,'pass'))	$obj->pass = $pwd;
$pwd = $obj->pass;
$obj->pass = password_hash($obj->pass,PASSWORD_BCRYPT,['cost'=>10]);
$dao->insertJSON("members",$obj);
if($dao->error){
	$result->success = false;
	$result->message = "Error adding Teacher";
}else{	$_					= New stdClass();
	$_->email		= $obj->email;
	$_->nom 		= $obj->name;
	$_->prenom 	= $obj->firstname;
	$_->ident 	= $obj->id;
	$_->pass 		= $pwd;
	$_->tel			= "";
	$_->gender	= "";
	
	$sendmail->newTeacher(array('ident'=>$obj->id,'prenom'=>$obj->firstname,'pass'=>$pwd,'email'=>$obj->email));
	
	$result->success = true;
	$result->message = $obj->id." added";
}

$root = "/home/www/vhosts/{$env->school}.imusic-school.info/app/teacher";
system("mkdir $root/{$obj->id}; cp $root/../student/template/avatar.png $root/{$obj->id}/;");

echo json_encode($result);
?>
