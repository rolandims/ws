<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra("SRV_CASSANDRA_IMS",$env->keyspace);
require_once($_SERVER['DOCUMENT_ROOT'].'/ws/common/alertMailFirebase.php');

$result = new stdClass();

# recup id classe
$obj = new stdClass();
if($_SERVER['QUERY_STRING']!='') $obj = json_decode($_SERVER['QUERY_STRING']);
else $obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if(!property_exists($obj,'frequency')) $obj->frequency='';
$end = date('Y-m-d H:i:s',strtotime($obj->end));
if($obj->frequency!=''){
	$obj->start= strtotime($obj->start);
	$obj->end= $end;
	$typeDuree = substr($obj->duration,0,1);
	$duree = floatval(substr($obj->duration,1));
	unset($obj->duration);
	$y = date('Y',$obj->start);
	$m = date('m',$obj->start);
	$d = date('d',$obj->start);
	$h = date('H',$obj->start);
	$i = date('i',$obj->start);
	$s = date('s',$obj->start);
	$start = "$y-$m-$d $h:$i:$s";
	$nb=0;
	$occurences = [];
	while($start <= $end){
		$nb++; if ($nb>53)break; # securité
		
		$_ = new Cassandra\UUID();
		$obj->id = "$_";
		
		$obj->start = date('Y-m-d H:i:s',mktime($h,$i,$s,$m,$d,$y));
		switch ($typeDuree) {
			case 'H':	$obj->end = date('Y-m-d H:i:s',mktime($h+$duree,$i,$s,$m,$d,$y));		break;
			case 'D':	$obj->end = date('Y-m-d H:i:s',mktime($h,$i,$s,$m,$d+$duree,$y));		break;
			case 'W':	$obj->end = date('Y-m-d H:i:s',mktime($h,$i,$s,$m,$d+7*$duree,$y));	break;
			case 'M':	$obj->end = date('Y-m-d H:i:s',mktime($h,$i,$s,$m+$duree,$d,$y));		break;
			case 'Y':	$obj->end = date('Y-m-d H:i:s',mktime($h,$i,$s,$m,$d,$y+$duree));		break;
		}
		$dao->insertJSON("workevents",$obj);
		
		$occurences[]="START:{$obj->start}, END:{$obj->end}, DUREE:$typeDuree-$duree";
		# suivant
		switch ($obj->frequency){
			case 'daily'		: $d++; 		break;
			case 'weekly'		: $d +=	7;	break;
			case 'fortnight':	$d +=	14;	break;
			case 'monthly'	:	$m++;			break;
			case 'yearly'		:	$y++;			break;
		}
		$start = date('Y-m-d H:i:s',mktime($h,$i,$s,$m,$d,$y));
	}
	$result->occurences = $occurences;
}else{
	$_ = new Cassandra\UUID();
	$obj->id = "$_";
	$dao->insertJSON("workevents",$obj);
}

if($dao->error){
	$result->success = false;
	$result->message = "Error adding WorkEvents";
	$result->msg = $dao->message;
	$result->obj = $obj;
	
}else{
	$result->success = true;
	$result->message = $obj->id." added";
	$result->id = $obj->id;
	//alertStudents($obj);

	$obj->title = "Agenda";
	$obj->date = $obj->start;
	$result->alerts = alert($obj,'classe',true,true);
}

echo json_encode($result);

function alertStudents($pObj){
	global $dao;
	require_once $_SERVER['DOCUMENT_ROOT']."/plugin/mailing/sendmail.php";
	$sendmail = new sendMail();
	
	$criterias = new stdClass();
	$criterias->id = $pObj->class;
	$_=$dao->find("classes","students,name",$criterias);
	foreach ($_['students'] as $k=>$v){
		$w=array($v);$v=(array($w[0]));
		foreach ($v as $a=>$b) {
			$c=(array)$b;
			$criterias->id = $c['values']['id'];
			$infoStd = $dao->find("members","email,name,firstname",$criterias);
			$sendmail->newEvent(array(
					"classe"	=>$_['name'],
					"date"		=>$pObj->start,
					"email"		=>$infoStd['email'],
					"prenom"	=>$infoStd['firstname'].' '.$infoStd['name'],
					"event"		=>$pObj->description,
					"title"		=>$pObj->title)
					);
		}
	}
	$datas = new StdClass();
	$datas->classId	= $pObj->class;
	$datas->title	= "Agenda ";
	$datas->content =	$pObj->title;
	$curl = curl_init("https://dev.imusic-school.info/firebase/sendMsg.js");
	curl_setopt($curl, CURLOPT_PORT,443);
	curl_setopt($curl, CURLOPT_POST, 1); 
	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($datas));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array("Connection: Close")); 

 	$response = curl_exec($curl);
 	curl_close($curl);
	$result->fire = $datas;	
	$result->fireResp = $response;

	
}
?>
