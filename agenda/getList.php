<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));


require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra("SRV_CASSANDRA_IMS",$env->keyspace);

$result = new stdClass();

# recup infos professeur

if(array_key_exists("id",$_GET)){
	$obj = new stdClass();
	$obj->id = $_GET['id'];
}else{
	$obj = json_decode(file_get_contents("php://input"));
}
if($obj==null ){die(200);}

if(($obj->id =='')||($obj->id == null)){
	$result->success = false;
	$result->message = "Missing ID";
	die(json_encode($result));
}

$_ = $dao->find("members","*",$obj);
if($_['profile']=='Teacher' || $_['profile']=='Admin'){
	$test = New stdClass();
	$test->admin = $obj->id;
	$result = json_encode($dao->findAll("workevents","*",$test));
}elseif($_['profile']=='Student'){
	
	$criteria = new StdClass();
	$events = [];
	$liste = $dao->executeTxt("SELECT id FROM classes WHERE students CONTAINS {id:'".$_['id']."',name:'".$_['name']."'} ALLOW FILTERING;");
	foreach ($liste as $classe){
		$criteria->class = "{$classe['id']}";
		$e = $dao->findAll("workevents","*",$criteria);
		if(($e=='') || ($e==null)){}else{$events[] = $e;}
	}
	$result = json_encode($events);
	$result=str_replace('[,','[',$result);
	$result=str_replace(',,',',',$result);
	$result=str_replace('],[',',',$result);
	$result=str_replace('},]','}]',$result);
	
	$result= substr($result,1,-1);
	if((substr($result,-1)==',')){$result= substr($result,1,-1);}
	if(($result=='[,]') || ($result=='[ ,]')) $result='';
}

echo $result;

?>
