<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$result = new stdClass();

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

$search = $obj->search;
$datas = $obj->datas;
if(!property_exists($search,"id")){
	$result->success = false;
	$result->message = "missing event id";
}else{
	$criterias = new stdClass();
	$criterias->id = "{$search->id}";
}



$reqTxt = $dao->getUpdateRequest("workevents",$datas);
$reqTxt = "UPDATE workevents SET $reqTxt WHERE id=".$search->id.";";	
$result->query = $reqTxt;
$dao->executeTxt($reqTxt);

if($dao->error){
	$result->success = false;
	$result->message = "Error updating event";
	$result->detail = $dao->message;
}else{
	$result->success = true;
	$result->message = $search->id." updated";
}
echo json_encode($result);
?>
