<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$result = new stdClass();

$criterias = json_decode(file_get_contents("php://input"));
if(property_exists($criterias,"id")){
	$_ = $dao->executeTxt("DELETE FROM notebooks WHERE id=".$criterias->id.";");

	if($dao->error){
		$result->success = false;
		$result->message = $dao->message;
	}else{
		$result->success = true;
		$result->message = $criterias->id." deleted ";
	}
}else{
		$result->success = false;
		$result->message = "Error datas sent";
}

echo json_encode($result);
?>