<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$result = new stdClass();

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

$search = $obj->search;
$datas = $obj->datas;
if(!property_exists($search,"id")){
	$result->success = false;
	$result->message = "missing note id";
}else{
	$criterias = new stdClass();
	$criterias->id = "{$search->id}";
}

$reqTxt = $dao->getUpdateRequest("notebooks",$datas);
$reqTxt = "UPDATE notebooks SET $reqTxt WHERE id=".$search->id.";";	
$result->query = $reqTxt;
$dao->executeTxt($reqTxt);

if($dao->error){
	$result->success = false;
	$result->message = "Error updating note";
	$result->detail = $dao->message;
}else{
	$result->success = true;
}
echo json_encode($result);
?>
