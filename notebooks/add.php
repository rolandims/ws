<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra("SRV_CASSANDRA_IMS",$env->keyspace);
require_once($_SERVER['DOCUMENT_ROOT'].'/ws/common/alertMailFirebase.php');

$result = new stdClass();
# recup id classe
$obj = new stdClass();
if($_SERVER['QUERY_STRING']!='') $obj = json_decode($_SERVER['QUERY_STRING']);
else 	$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

$_ = new Cassandra\UUID();
$obj->id = "$_";

$dao->insertJSON("notebooks",$obj);
if($dao->error){
	$result->success = false;
	$result->message = "Error adding notebook item";
	$result->msg = $dao->message;
  $result->obj = $obj;
}else{
	$result->success = true;
	$result->message = $obj->id." added";
	$result->id = $obj->id;
	//alertStudents($obj);

	$obj->title = "Note";
	$obj->date = $obj->notedate;
	$result->alerts = alert($obj,'classe',true,true);
}

echo json_encode($result);

function alertStudents($obj){
	global $dao, $result;
	require_once $_SERVER['DOCUMENT_ROOT']."/plugin/mailing/sendmail.php";
	$sendmail = new sendMail();

	$criterias = new stdClass();
	$criterias->id = $obj->classId;
	$_=$dao->find("classes","students,name",$criterias);
	foreach ($_['students'] as $k=>$v){
		$w=array($v);$v=(array($w[0]));
		foreach ($v as $a=>$b) {
			$c=(array)$b;
			$criterias->id = $c['values']['id'];
			$infoStd = $dao->find("members","email,name,firstname",$criterias);
			$sendmail->newEvent(array(
					"email"=>$infoStd['email'],
					"prenom"=>$infoStd['firstname'].' '.$infoStd['name'],
					"classe"=>$_['name'],
					"date"=>$obj->notedate,
					"event"=>$obj->description,
					"title"=>"Note"
					));
		}
	}
	//mode get 
	//	$firebase = file_get_contents("https://dev.imusic-school.info/firebase/sendMsg?classId=".$obj->classId."&title=Note&content=".urlencode($obj->description));
	//mode post
	$datas = new StdClass();
	$datas->classId	= $obj->classId;
	$datas->title	= "Note";
	$datas->content =	$obj->description;
	$curl = curl_init("https://dev.imusic-school.info/firebase/sendMsg.js");
	curl_setopt($curl, CURLOPT_PORT,443);
	curl_setopt($curl, CURLOPT_POST, 1); 
	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($datas));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array("Connection: Close")); 

	$response = curl_exec($curl);
	curl_close($curl);
	$result->fire = $datas;	
	$result->fireResp = $response;
}

?>
