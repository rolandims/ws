<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra("SRV_CASSANDRA_IMS",$env->keyspace);

$result = new stdClass();

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}
/*
if(($obj->classId =='')||($obj->classId == null)){
	$result->success = false;
	$result->message = "Missing ID";
	die(json_encode($result));
}*/
$_ = $dao->executeTxt("SELECT * FROM notebooks WHERE classid='{$obj->classId}';");
$return = array();
foreach ($_ as $k) {
	array_push($return,$k);
}
echo json_encode($return);

?>