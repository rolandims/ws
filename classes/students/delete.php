<?php 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$result = new stdClass();

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if (!isset($obj->classId) || !isset($obj->studentId)){
    $result->success = false;
    $result->message = 'NO DATAS';
    echo json_encode($result);die();
}

$criterias = new stdClass();
$criterias->id = $obj->classId; 
$_=$dao->find("classes","students",$criterias);
foreach ($_['students'] as $k=>$v){
	$w=array($v);$v=(array($w[0]));
	foreach ($v as $a=>$b) {
		$c=(array)$b;
		if($c['values']['id']==$obj->studentId){
			$dao->executeTxt ("UPDATE classes SET students = students - [{id:'".$obj->studentId."',name : '".$c['values']['name']."'}] WHERE id='{$obj->classId}';");
			//$dao->executeTxt("DELETE FROM studentteacher WHERE studentId='{$obj->studentId}' AND teacherId='{$_GET['id']}';");
		}
	}
}

$result->success = !$dao->error;
$result->message = $dao->message;
echo json_encode($result);die();
?>