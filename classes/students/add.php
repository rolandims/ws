<?php 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

require_once($_SERVER['DOCUMENT_ROOT'].'/ws/common/alertMailFirebase.php');

$result = new stdClass();

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if (!isset($obj->classId) || !isset($obj->studentId) || !isset($obj->studentName)){
    $result->success = false;
    $result->message = 'NO DATAS';
    echo json_encode($result);die();
}

$classId = $obj->classId;
$_ = $dao->executeTxt("SELECT teacher FROM classes WHERE id='$classId' ALLOW FILTERING;");
$teacherId = $_[0]['teacher'];

$studentId = $obj->studentId;
$studentName = $obj->studentName;
$req = "UPDATE classes SET students = students+ [{id:'".$studentId."',name : '$studentName'"."}] WHERE id='$classId';";
$dao->executeTxt($req);
$dao->executeTxt("INSERT INTO studentteacher (studentId,teacherId) VALUES ('$studentId','$teacherId');");

$result->success = !$dao->error;
$result->message = $dao->message;

$obj->title 			= "Nouvel élève";
$obj->description = "L'élève {$obj->studentName} à été ajouté à votre classe {$_['name']}";
$obj->date 				= date('d/m/Y');
$result->alerts = alert($obj,'teacher',false,true);

echo json_encode($result);
die();

function alertTeacher($obj){
	global $dao, $result;
	require_once $_SERVER['DOCUMENT_ROOT']."/plugin/mailing/sendmail.php";
	$sendmail = new sendMail();

	$criterias = new stdClass();
	$criterias->id = $obj->classId;
	$_=$dao->find("classes","teacher,name",$criterias);
	$criterias->id = $_['teacher'];
	$infoStd = $dao->find("members","email,name,firstname",$criterias);
	$sendmail->newEvent(array(
	    "email"=>$infoStd['email'],
		"prenom"=>$infoStd['firstname'].' '.$infoStd['name'],
		"classe"=>$_['name'],
		"date"=>date('d/m/Y'),
		"event"=>"L'élève a été ajouté à votre classe {$_['name']}.",
		"title"=>"Nouvel élève"
	));
		
$datas = new StdClass();
	$datas->ident	= $_['teacher'];
	$datas->title	= "Nouvel élève";
	$datas->content =	"L'élève {$obj->studentName} à été ajouté à votre classe {$_['name']}";
$curl = curl_init("https://dev.imusic-school.info/firebase/sendMsg.js");
curl_setopt($curl, CURLOPT_PORT,443);
curl_setopt($curl, CURLOPT_POST, 1); 
curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($datas));
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER, array("Connection: Close")); 

// $response = curl_exec($curl);
 curl_close($curl);
$result->fire = $datas;	
$result->fireResp = $response;
}

?>
