<?php 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$result = new stdClass();

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if (!isset($obj->classId) || !isset($obj->studentList)){
    $result->success = false;
    $result->message = 'NO DATAS';
    echo json_encode($result);die();
}

$classId = $obj->classId;
$_ = $dao->executeTxt("SELECT teacher FROM classes WHERE id='$classId' ALLOW FILTERING;");
$teacherId = $_[0]['teacher'];
foreach ($obj->studentList as $value) {
    $studentId = $value->id;
    $studentName = $value->name;
    $req = "UPDATE classes SET students = students+ [{id:'".$studentId."',name : '$studentName'"."}] WHERE id='$classId';";
    $dao->executeTxt($req);
    $dao->executeTxt("INSERT INTO studentteacher (studentId,teacherId) VALUES ('$studentId','$teacherId');");
}

$result->success = !$dao->error;
$result->message = $dao->message;
$result->datasJSON = $req;
echo json_encode($result);die();
?>
