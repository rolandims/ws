<?php 
/// Créé une liste de classe depuis csv instruId,teacherId,studentId à élève unique
die();

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra("SRV_CASSANDRA_IMS",$env->keyspace);

$files = file_get_contents($_SERVER['DOCUMENT_ROOT']."/adma.csv");

foreach(explode("\n",$files) as $line){
    list($instru,$teacherId,$studentId)  = explode(",",str_replace("\r","",$line));
    $student = $dao->executeTxt("SELECT name,firstname FROM members WHERE id='$studentId' ALLOW FILTERING; ");
    $obj = New StdClass();
    $obj->id = uniqid (rand (), true);
    $obj->name = $student[0]['name'];
    $obj->description = $student[0]['name']." ".$student[0]['firstname'];
    $obj->instrument = $instru;
    $obj->level = 1;
    $obj->notebookid = "";
    $obj->teacher = $teacherId;
    $obj->type = "";
    $studentObj = New StdClass();
    $studentObj->id = $studentId;
    $studentObj->name = $student[0]['name'];
    $obj->students = [$studentObj];
    echo $instru." - ".$teacherId." - ".$studentId."<br>";
    $dao->insertJSON("classes",$obj);
}


?>
