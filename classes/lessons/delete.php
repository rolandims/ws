<?php 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$result = new stdClass();

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if (!isset($obj->classId) || !isset($obj->lesson)){
    $result->success = false;
    $result->message = 'NO DATAS';
    echo json_encode($result);die();
}

$classId = $obj->classId;
$lessonId = $obj->lesson;

$req = "UPDATE classes SET lessons = lessons - ['$lessonId'] WHERE id='$classId';";

$dao->executeTxt($req);

$result->success = !$dao->error;
$result->message = $dao->message;
$result->datasJSON = $req;


echo json_encode($result);
?>
