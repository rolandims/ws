<?php 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);
require_once($_SERVER['DOCUMENT_ROOT'].'/ws/common/alertMailFirebase.php');

$result = new stdClass();

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if (!isset($obj->classId) || !isset($obj->lesson)){
    $result->success = false;
    $result->message = 'NO DATAS';
    echo json_encode($result);die();
}

$classId = $obj->classId;
$lessonId = $obj->lesson;

$req = "UPDATE classes SET lessons = lessons + ['$lessonId'] WHERE id='$classId';";

$dao->executeTxt($req);

$result->success = !$dao->error;
$result->message = $dao->message;
$result->datasJSON = $req;

//alertStudents($obj);
$obj->title 			= "Cours";
$obj->description = "Un nouveau cours vient d'être ajouté à votre classe, connectez vous pour le consulter";
$obj->date 				= date('d/m/Y');
$result->alerts = alert($obj,'classe',true,true);

echo json_encode($result);

function alertStudents($obj){
	global $dao, $result;
	require_once $_SERVER['DOCUMENT_ROOT']."/plugin/mailing/sendmail.php";
	$sendmail = new sendMail();

	$criterias = new stdClass();
	$criterias->id = $obj->classId;
	$_=$dao->find("classes","students,name",$criterias);
	foreach ($_['students'] as $k=>$v){
		$w=array($v);$v=(array($w[0]));
		foreach ($v as $a=>$b) {
			$c=(array)$b;
			$criterias->id = $c['values']['id'];
			$infoStd = $dao->find("members","email,name,firstname",$criterias);
			$sendmail->newEvent(array(
					"email"=>$infoStd['email'],
					"prenom"=>$infoStd['firstname'].' '.$infoStd['name'],
					"classe"=>$_['name'],
					"date"=>date("d/m/Y"),
					"event"=>"Un nouveau cours vient d'être ajouté à votre classe, connectez vous pour le consulter",
					"title"=>"Cours"
					));
		}
	}

   $datas = new StdClass();
	$datas->classId	= $obj->classId;
	$datas->title	= "Cours";
	$datas->content =	"Un nouveau cours vient d'être ajouté à votre classe, connectez vous pour le consulter";
	$curl = curl_init("https://dev.imusic-school.info/firebase/sendMsg.js");
	curl_setopt($curl, CURLOPT_PORT,443);
	curl_setopt($curl, CURLOPT_POST, 1); 
	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($datas));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array("Connection: Close")); 

	$response = curl_exec($curl);
	curl_close($curl);
	$result->fire = $datas;	
	$result->fireResp = $response;
}

?>
