

<?php 
/*   KEYSPACES 
TABLE ecoles.classes (
    id text PRIMARY KEY,
    instrument int,
    level int,
    notebookid text,
    teacher text,
    type text,
    documents list<text>,
    lessons list<text>,
    students list<text>
)
*/

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$result = new stdClass();

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

$obj->id = uniqid (rand (), true);

$dao->insertJSON("classes",$obj);
if($dao->error){
	$result->success = false;
	$result->message = "Error adding classes";
	$result->msg = $dao->message;
  $result->obj = $obj;
}else{
	# ajout room classe
	$rObj=new StdClass();
	$rObj->roomId= $obj->id;
	$rObj->adminId = $obj->teacher;
	$rObj->isActive = true;
	$rObj->name = $obj->name;
	$rObj->instrument = $obj->instrument;
	$rObj->usersList = '["'.$obj->teacher.'"]';
	$dao->insertJSON("rooms",$rObj);
	
	$result->success = true;
	$result->message = $obj->id." added";
	$result->id = $obj->id;
}
echo json_encode($result);
?>
