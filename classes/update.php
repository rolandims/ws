

<?php 
/*   KEYSPACES 
TABLE ecoles.classes (
    id text PRIMARY KEY,
    instrument int,
    level int,
    notebookid text,
    teacher text,
    type text,
    documents list<text>,
    lessons list<text>,
    students list<text>
)
*/

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$result = new stdClass();

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

$search = $obj->search;
$datas = $obj->datas;
if (isset($datas->documents)){
    $datas->documents = editListValue($datas->documents);
}
if (isset($datas->lessons)){
    $datas->lessons = editListValue($datas->lessons);
}
if (isset($datas->students)){
    $datas->students = editListValue($datas->students);
}

$req = "UPDATE classes SET ";
foreach ((array)$datas as $k=>$v) {
    if($k=='students'||$k=='documents'||$k=='lessons'){
        $req	.= " $k = $v , "; 
    }else{
        if(is_string($v)){
            $req	.= " $k = '$v' , ";	 
        }else{
            $req	.= " $k = $v , ";	 
        }
       
    }
   	
}
$req = substr($req,0,-2);
$req .=" WHERE ";
foreach ((array)$search as $k=>$v) {
    if(is_string($v)){
        $req	.= " $k = '$v' AND ";	
    }else{
        $req	.= " $k = $v AND ";
    }
    		
}
$req = substr($req,0,-4);

// UPDATE classes SET  description = 'zerty TEST' ,  documents = [] ,  instrument = 2 ,  lessons = [] ,  level = 0 ,  name = 'azerty' ,  notebookid = '' ,  students = ['student1','student2'] ,  teacher = 'rolandimusic' ,  type = ''  WHERE  id = '780637009610d2ac0dbe7a1.39103685' ;

$dao->executeTxt($req.";");

$result->success = !$dao->error;
$result->message = $dao->message;
$result->datasJSON = $req;

function editListValue($p){
    $_ = "";
    foreach ($p as $v){$_ .="'$v',";}
	return '['.substr($_,0,-1).']';
}


echo json_encode($result);
?>
