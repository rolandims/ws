<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT'].'/ws/objClasses.php';
$class = new Classes($env->keyspace);

$result = new stdClass();

# recup id classe
$obj = new stdClass();
if(array_key_exists("id",$_GET)) $obj->id = $_GET['id'];
else 	$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if(($obj->id =='')||($obj->id == null)){
	$result->success = false;
	$result->message = "Missing Class ID";
}
$_ = $class->infoClass($obj->id);
$docs  =[];
	foreach ($_['documents'] as $doc){
		$infos = pathinfo($doc);
		if(array_key_exists('dirname',$infos)){
			$dirname=explode('/',$infos['dirname']);
			$sub=array_pop($dirname);
			$docs[]=array("url"=>$doc,"folder"=>$sub,"name"=>$infos['filename'],"type"=>$infos['extension']);
		}
	}
$_['documents']=$docs;
echo json_encode($_);
?>