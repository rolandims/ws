<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT'].'/ws/objClasses.php';
$class = new Classes($env->keyspace);

$result = new stdClass();

# recup id classe
$obj = new stdClass();
if(array_key_exists("id",$_GET)) $obj->id = $_GET['id'];
else 	$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if(($obj->id =='')||($obj->id == null)){
	$result->success = false;
	$result->message = "Missing Class ID";
}
$_ = $class->infoClass($obj->id);
$docs  =[];
foreach ($_['documents'] as $doc){
 $infos = pathinfo($doc);
 $sub=array_pop(explode('/',$infos['dirname']));
 $docs[]="{name:'$doc',folder:'$sub'}";
 }
 $_['documents']=$docs;
echo json_encode($_);
?>