<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra("SRV_CASSANDRA_IMS",$env->keyspace);

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}


if(array_key_exists("id",$obj)){
	$_ = $dao->find("members","*",$obj);
	if($_['profile'] =='Teacher' ){
		$criteria = New StdClass();
		$criteria->teacher = $obj->id;
		$classes =  $dao->findAll("classes","*",$criteria);
		echo json_encode($classes);
	}elseif($_['profile'] =='Admin'){
		$criteria = New StdClass();
		$criteria->teacher = $obj->id;
		$classes =  $dao->findAll("classes","*",$criteria);
		echo json_encode($classes);
	}else{
		$query="SELECT * FROM classes WHERE students CONTAINS {id:'".$_['id']."',name:'".$_['name']."'} ALLOW FILTERING;";
		$classes =  $dao->executeTxt($query);
		$result=[];
		foreach ($classes as $class){
			$result[]=$class;
		}
		echo json_encode($result);
	}
}
?>
