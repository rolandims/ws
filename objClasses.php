<?php 
require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";

class Classes {
	
	public $school;
	private $dao;
	public $error;
	public $message;
	
	function __construct($pSchool){
		$this->error = false;
		$this->message = "";
		$this->dao = new daoCassandra('SRV_CASSANDRA_IMS',$pSchool);
	}
	
	function create($pClassName,$pTeacher,$pInstrument, $pLevel, $pType, $pStudents) {
		$json = new stdClass();
		$json->id = $pClassName;
		$json->instrument = $pInstrument;
		$json->level = $pLevel;
		$json->type = $pType;
		$json->teacher = $pTeacher;
		$students = '[';
		foreach ($pStudents as $student){$students.="\"$student\",";}
		$json->students = substr($students,0,-1).']';
echo var_export($json);		
		$this->dao->insertJSON("classes",$json);
	}
	
	function addStudents($pClassName,$pStudents){
		$criteria = new stdClass();
		$criteria->id = $pClassName;
		$_ = $this->dao->find("classes","students",$criteria);
		$json = new stdClass();
		$json->students = '[';
		foreach ($pStudents as $student){$json->students .="'$student',";}
		$json->students = substr($json->students,0,-1).']';
		$this->dao->updateMap("classes",$json,$criteria);
		
		$this->error = $this->dao->error;
		$this->message = $this->dao->message;
	}
	
	function removeStudents($pClassName,$pStudent){
		$json = new stdClass();
		$json->students = '{'.implode(',',$pStudent).'}';
		
		$criteria = new stdClass();
		$criteria->id = $pClassName;
		$this->dao->deleteMap("classes",$json,$criteria);
		$this->error = $this->dao->error;
		$this->message = $this->dao->message;
	}
	
	function listStudents($pClassName){
		$criteria = new stdClass();
		$criteria->id = $pClassName;
		$_ = $this->dao->find("classes","students",$criteria);
		return $_['students'];
	}
	
	function infoClass($pClassName){
		$criteria = new stdClass();
		$criteria->id = $pClassName;
		$class =  $this->dao->find("classes","*",$criteria);
		$students = [];
		if($class['students']!=null){
			foreach ($class['students'] as $s) {
				$students[] = $s;
			}
		}
		$class['students'] = $students;
		if(($class['documents']==null) || ($class['documents']=='')) $class['documents']=[];
		return $class;
	}
	
	
	function createDiary($pClassName){
		;
	}
	
	function updateDiary($pClassName){
		;
	}
	
	function showDiary($pClassName){
		;
	}
	
	function removeDiary($pClassName){
		
	}
	
	function createEvent($pEvent){
		# table workevents id,class,label, description, start, end 
		if(!property_exists($pEvent,'id') || $pEvent->id==null){
			$id = new Cassandra\UUID(); 
			//echo "<pre>";print_r ($id);echo "</pre>";#die($id);
			$pEvent->id = "$id";
		}
		$this->dao->insertJSON("workevents",$pEvent);
		$this->error = $this->dao->error;
		$this->message = $this->dao->message;
		$this->eventId = $pEvent->id;
		
	}
	
	function deleteEvent($pClassName){
	
		;
	}
	
	function listEvent($pClassName){
		;
	}
}
?>