<?php 

$sentDatas = json_decode(file_get_contents("php://input"));

$ret = New StdClass();
$ret->success   = true;
$ret->msg       = "";

if($sentDatas==null ){
    $ret->success   = false;
    $ret->msg       = "No Datas Sent";
    echo json_encode($ret);
    die();
}

if(!property_exists($sentDatas,"lesson")){
    $ret->success   = false;
    $ret->msg       = "Missing data lesson";
    $ret->sd        = $sentDatas;
    echo json_encode($ret);
    die();
}else{
    $lesson = $sentDatas->lesson;
}
if(!property_exists($sentDatas,"seq")){
    $ret->success   = false;
    $ret->msg       = "Missing data seq";
    die();
}else{
    $seq = $sentDatas->seq;
}
require_once ($_SERVER["DOCUMENT_ROOT"] . '/plugin/connect/Dao.php');
$dao		= new Dao('TV','lesson');
$datas      = $dao->find("SELECT l.sequence AS seqNum ,l.description AS seqTitle ,c.chapitre AS chapNum,c.libelle AS chapTitle,s.souschapitre AS sousChapNum,s.libelle AS sousChapTitle FROM $lesson l,chapitres c,schapitres s WHERE l.seq=$seq AND (l.chapitre = c.chapitre AND c.lesson='$lesson') AND (l.chapitre = s.chapitre AND l.souschapitre=s.souschapitre AND s.lesson='$lesson'); ");

$ret->chapNum       = $datas['chapNum'];
$ret->chapTitle     = $datas['chapTitle'];
$ret->sousChapNum   = $datas['sousChapNum'];
$ret->sousChapTitle = $datas['sousChapTitle'];
$ret->seqNum        = $datas['seqNum'];
$ret->seqTitle      = $datas['seqTitle'];
echo json_encode($ret);
?>