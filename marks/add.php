<?php 
$result = New StdClass();
$result->success = false;

$sentDatas = json_decode(file_get_contents("php://input"));
if($sentDatas==null ){ 
    $result->msg = "Param null !";
    echo json_encode($result);
    die(418);
}
if(property_exists($sentDatas,"studentId")){
    $studentId = $sentDatas->studentId;
    $studentFileName    = $_SERVER['DOCUMENT_ROOT']."/app/student/$studentId/notes.json";
}else{
    $result->msg = "Paramètre d'identification de l'élève manquant !";
    echo json_encode($result);
    die(418);
}


if(property_exists($sentDatas,"noteId")){
    $noteId = $sentDatas->noteId;
}else{
    $result->msg = "Paramètre d'identification de note manquant !";
    echo json_encode($result);
    die(418);
}
if(property_exists($sentDatas,"marks")){
    $marks = $sentDatas->marks;
}else{
    $result->msg = "Aucune note !";
    echo json_encode($result);
    die(418);
}
if(property_exists($sentDatas,"date")){
	$date = $sentDatas->date;
}else{
	$date = date('Y-m-d');
}
$result->success = true;
if(property_exists($sentDatas,"teacherId")){
    $teacherId = $sentDatas->teacherId;
}else{
    $teacherId = "";
}
if(property_exists($sentDatas,"lesson")){
    $lesson = $sentDatas->lesson;
}else{
    $lesson = "";
}
if(property_exists($sentDatas,"seq")){
    $seq = $sentDatas->seq;
}else{
    $seq = 0;
}
if(property_exists($sentDatas,"desc")){
    $desc = $sentDatas->desc;
}else{
    $desc = "";
}



if(!file_exists($studentFileName)){
    $noteList = [];
}else{
    $noteList = json_decode(file_get_contents($studentFileName));
}

$value = New StdClass();
$value->teacherId = $teacherId;
$value->id        = time();
$value->marks     = $marks;
$value->noteId    = $noteId;
$value->date      = $date;
$value->lesson    = $lesson;
$value->seq       = $seq;
$value->desc      = $desc;

$noteList[] = $value;
file_put_contents($studentFileName,json_encode($noteList));
echo json_encode($result);
die();
?>