<?php 
$sentDatas = json_decode(file_get_contents("php://input"));
if($sentDatas==null ){die(200);}
if(property_exists($sentDatas,"studentId")){
    $studentId = $sentDatas->studentId;
    $studentFileName    = $_SERVER['DOCUMENT_ROOT']."/app/student/$studentId/notes.json";
}else{
    echo json_encode("No parameters");
    die(200);
}

if(!file_exists($studentFileName)){
    echo json_encode([]);
    die();
}

if(property_exists($sentDatas,"start")){
    $start = $sentDatas->start;
}else{
    echo json_encode(json_decode(file_get_contents($studentFileName)));
    die();
}

if(property_exists($sentDatas,"end")){
    $end = $sentDatas->end;
}else{
    echo json_encode(json_decode(file_get_contents($studentFileName)));
    die();
}

require_once ($_SERVER["DOCUMENT_ROOT"] . '/plugin/connect/Dao.php');
$dao		= new Dao('TV','lesson');

$noteList = json_decode(file_get_contents($studentFileName));
$returnList = [];
foreach ($noteList as $value) {
    if($value->date>=$start && $value->date<=$end ){
        if($value->marks<0){
            $value->marks = 0;
        }else{
            $value->marks = round($value->marks,2);  
        }
        if(property_exists($value,"lesson") && property_exists($value,"seq")){
            $value->detail      = getDetail($value->lesson,$value->seq);
        }else{
            $value->detail      = null;
        }
        if(!property_exists($value,"desc")){
            $value->desc        = "";
        }
       
        $returnList[] = $value;
    }
}
echo json_encode($returnList);

function getDetail($lesson,$seq){
    global $dao;
    $datas = $dao->find("SELECT l.sequence AS seqNum ,l.description AS seqTitle ,c.chapitre AS chapNum,c.libelle AS chapTitle,s.souschapitre AS sousChapNum,s.libelle AS sousChapTitle FROM $lesson l,chapitres c,schapitres s WHERE l.seq=$seq AND (l.chapitre = c.chapitre AND c.lesson='$lesson') AND (l.chapitre = s.chapitre AND l.souschapitre=s.souschapitre AND s.lesson='$lesson'); ");
    $r = New StdClass();
    $r->seqNum = $datas['seqNum'];
    $r->seqTitle = $datas['seqTitle'];
    $r->chapNum = $datas['chapNum'];
    $r->chapTitle = $datas['chapTitle'];
    $r->sousChapNum = $datas['sousChapNum'];
    $r->sousChapTitle = $datas['sousChapTitle'];
    return $r;
}

?>