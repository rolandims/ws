<?php 
$result = New StdClass();
$result->success = false;

$sentDatas = json_decode(file_get_contents("php://input"));
if($sentDatas==null ){ 
    $result->msg = "Param null !";
    echo json_encode($result);
    die(418);
}
if(property_exists($sentDatas,"studentId")){
    $studentId = $sentDatas->studentId;
    $studentFileName    = $_SERVER['DOCUMENT_ROOT']."/app/student/$studentId/notes.json";
}else{
    $result->msg = "Paramètre d'identification de l'élève manquant !";
    echo json_encode($result);
    die(418);
}


if(property_exists($sentDatas,"id")){
    $id = $sentDatas->id;
}else{
    $result->msg = "Paramètre d'identification de note manquant !";
    echo json_encode($result);
    die(418);
}


if(!file_exists($studentFileName)){
    $result->msg = "Fichier manquant!";
    echo json_encode($result);
    die(418);
}else{
    $noteList = json_decode(file_get_contents($studentFileName));
}
$result->success = true;

for ($i=0; $i < count($noteList); $i++) { 
   if($noteList[$i]->id==$id){
    array_splice($noteList, $i, 1);
    //unset($noteList[$i]);
    break;
   }
}
file_put_contents($studentFileName,json_encode($noteList));
echo json_encode($result);
die();
?>