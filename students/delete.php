<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$result = new stdClass();
$obj = new stdClass();

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

# verif droit suppression: type admin uniquement
$criterias = new stdClass();
$criterias->id = $obj->user;
$_ = $dao->find("members","profile,roles",$criterias);
if(($_['profile']=='Admin')||(($_['profile']=='Teacher')&& ($_['roles']==3))){
	$result->success = true;
}else{
	$result->success = false;
	$result->message = "Only Administrators can delete a student";
	die(json_encode($result));
}

# recup nom eleve
$criterias = new StdClass();
$criterias->id = "{$obj->id}";
$criterias->profile = "Student";
$_ = $dao->find("members","id,name",$criterias);
$name = $_['name'];
$id = $_['id'];

# recherche les classes de l'élève
$criterias = new stdClass();
$criterias->students = "{id:'".$id."',name:'".$name."'}";
$query ="SELECT id FROM classes; ";
$_ = $dao->executeTxt($query);
foreach ($_ as $class) {
	$dao->executeTxt("update classes set students = students -[{id:'".$id."',name:'$name'}] where id = '".$class['id']."';");
}

# suppression de la table des liens
$_ = $dao->executeTxt("DELETE FROM studentteacher WHERE studentId='$id';");

# suppression de la table members
$_ = $dao->executeTxt("DELETE FROM members WHERE id='$id';");

if($dao->error){
	$result->success = false;
	$result->message = "Error deleting member";
}else{
	$result->success = true;
	$result->message = $id." deleted from members and classes";
}


echo json_encode($result);
?>

