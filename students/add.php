<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));
require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$result = new stdClass();

$obj= json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if(!isset($obj->id)){
	$result->success = false;
	$result->message = "Error send data id";
	echo json_encode($result);die();
}else{
	$criteria = new StdClass();
	$criteria->id = $obj->id;
	$_ = $dao->find("members","id",$criteria);
	if ($_['id']==$obj->id){
		$result->success = false;
		$result->message = "ID already in use";
		echo json_encode($result);die();
	}
}

if(!isset($obj->email)){
	$result->success = false;
	$result->message = "Error send data email";
	echo json_encode($result);die();
	
}else{
	$obj->email = strtolower($obj->email);
	$criteria = new StdClass();
	$criteria->email = $obj->email;
	$_ = $dao->find("members","email",$criteria);
	if ($_['email']==$obj->email){
		$result->success = false;
		$result->message = "email exists";
		echo json_encode($result);die();
	}
	
}
if(!property_exists($obj,'valid')) $obj->valid = true;
if(!property_exists($obj,'roles')) $obj->roles = 0;


require_once $_SERVER['DOCUMENT_ROOT']."/plugin/mailing/sendmail.php";
$sendmail = new sendMail();

# cryptage pass
$p1					= array ('do','re','mi','sol','fa','la','si');
$p2					= array ('ronde','croche','noire','ton');
$v3 				= round ( rand ( 0, 99 ), 0 );
$v1 				= array_rand ( $p1 );
$v2 				= array_rand ( $p2 );
$pwd 				= $p1 [$v1] . $v3 . $p2 [$v2];

$obj->profile		= "Student";
$obj->pass			= password_hash($pwd,PASSWORD_BCRYPT,['cost'=>10]);


$obj->profile = 'Student';
$dao->insertJSON("members",$obj);

if(array_key_exists("id",$_GET)) $dao->executeTxt("INSERT INTO studentteacher (studentId,teacherId) VALUES ('{$obj->id}','{$_GET['id']}');");

/*$dao->executeTxt("INSERT INTO members (id) VALUES('{$obj->id}');");
$reqTxt = $dao->getValueRequest("members",$obj);
$reqTxt = "UPDATE members SET $reqTxt WHERE id='{$obj->id}';";
$result->query = $reqTxt;
$dao->executeTxt($reqTxt);
*/
if($dao->error){
	$result->success = false;
	$result->message = $dao->message;
}else{
	$_					= New stdClass();
	$_->email		= $obj->email;
	$_->nom 		= $obj->name;
	$_->prenom 	= $obj->firstname;
	$_->ident 	= $obj->id;
	$_->pass 		= $pwd;
	$_->tel			= "";
	$_->gender	= "";
	
	$sendmail->newStudent(array('ident'=>$obj->id,'prenom'=>$obj->firstname,'pass'=>$pwd,'email'=>$obj->email));
	$result->success = true;
	$result->message = $obj->id." added";
	
	$root = "/home/www/vhosts/{$env->school}.imusic-school.info/app/student";
	
	system("cp -r $root/template $root/{$_->ident};");
	
}
echo json_encode($result);
?>
