<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));
require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$result = new stdClass();

$obj= json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

// pour tests
//$obj = new stdClass();
//$obj->id=$_GET['id'];
if(!isset($obj->id)){
	$result->success = false;
	$result->message = "Error send data id";
	echo json_encode($result);die();
}else{
	$criteria = new StdClass();
	$criteria->id = $obj->id;
	$_ = $dao->find("members","id",$criteria);
	
	if ($_['id']==$obj->id){
		$end = date('Y-m-d', strtotime('+'.$obj->ttl.' days'));

		$dao->executeTxt("INSERT INTO authorizations (userId,tokenId,endValid) VALUES('{$obj->id}','{$obj->tokenid}','$end');");		
		$result->success  = true;
		$result->message  = "tokenId added";
	}else{
		$result->success = false;
		$result->message = "ID not found";
	}
}

echo json_encode($result);
?>
