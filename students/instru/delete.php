<?php 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");

$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new daoCassandra('SRV_CASSANDRA_IMS',$env->keyspace);

$result = new stdClass();

$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if (!isset($obj->id) || !isset($obj->instru)){
    $result->success = false;
    $result->message = 'NO DATAS';
    echo json_encode($result);die();
}

$id = $obj->id;
$instru = $obj->instru;

$req = "UPDATE members SET instrument = instrument - {'$instru'} WHERE id='$id';";

$dao->executeTxt($req);

$result->success = !$dao->error;
$result->message = $dao->message;
$result->datasJSON = $req;

echo json_encode($result);
?>

