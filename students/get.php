<?php 
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: *");
$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/env.json"));

require_once $_SERVER['DOCUMENT_ROOT']."/plugin/connect/DaoCassandra.php";
$dao = new DaoCassandra("SRV_CASSANDRA_IMS",$env->keyspace);

$result = new stdClass();
$result->success	= true;
# recup infos élève
$obj = new stdClass();
if(array_key_exists("id",$_GET)) $obj->id = $_GET['id'];
else 	$obj = json_decode(file_get_contents("php://input"));
if($obj==null ){die(200);}

if(($obj->id =='')||($obj->id == null)){
	$result->success = false;
	$result->message = "Missing Student ID";
}
$result->student = $dao->find("members","id,birthdate,name,firstname,email,connected,gender,phone,favlist,scorefav,valid,address,contact,plfav,instruments",$obj);
$result2 = $dao->find("members","contact2",$obj);
$result->student['contact2']=$result2['contact2'];
unset($result->student->{"pass"});
echo json_encode($result);
?>